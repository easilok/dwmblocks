/* #define CPU_CMD		"top -b -n 1 | head -n1 | awk '{ print $(NF - 2)}' | sed 's/,//'" */
#define CPU_CMD		"~/.local/bin/dwmblocks/scripts/get-cpu" 
#define RAM_CMD		"free | awk '/^Mem/ { printf (\"%d%\"), $3/$2 * 100}'"
#define TEMP_CMD	"cat /sys/class/thermal/thermal_zone0/temp | awk '{printf(\"%dºC\"), $1/1000}'"
#define DATE_CMD	"date '+%d/%b - %H:%M'"
#define MAIL_CMD    "~/.local/bin/dwmblocks/scripts/get-mail"
#define KB_CMD		"setxkbmap -query | awk '/^layout/ { print $2 }'"
#define VOL_CMD		"~/.local/bin/dwmblocks/scripts/get-volume"
#define BAT_CMD		"~/.local/bin/dwmblocks/scripts/get-bat2"
#define MUS_CMD		"~/.local/bin/dwmblocks/scripts/get-playing"
#define RAM2_CMD    "free | awk '/^Mem/ { printf (\"%d%\"), 100 - ($7/$2 * 100)}'"
#define DISK_CMD    "df / | grep '/dev' | awk '{print $5}'"

//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0}, */
	/* {"",	MUS_CMD,	10,		0}, */
	{"",	BAT_CMD,	10,		0},
	{"",	VOL_CMD,	5,		11},
	{"",	KB_CMD,		10,		0},
	{"P: ", CPU_CMD,	5,		0},
	{"M: ", RAM2_CMD,	5,		0},
	{"/: ", DISK_CMD,	600,	0},
	{"T: ", TEMP_CMD,	5,		0},
	{"",	MAIL_CMD,	120,	0},
	{"",	DATE_CMD,	60,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
